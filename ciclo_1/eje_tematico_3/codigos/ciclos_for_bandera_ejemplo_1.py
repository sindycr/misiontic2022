"""
Estructuras iterativas y variables de control en Python
Imprimir los números enteros dentro de determinado rango
de valores mediante un ciclo for, y determina si dentro
de ese rango de valores se encuentra cierto número
"""
# definición de bandera
bandera = False
# valor inicial de rango de valores
inicio = 6
# valor final de rango de valores
final = 21
# número que se busca
valor = 4
# ciclo for
for i in range(inicio, final):
    # estructura de control condicional
    # determina si i en algún momento toma el valor del "valor"
    if i == valor:
        # se cambia el valor de la bandera
        bandera = True
    # imprime variable i
    print(i)
