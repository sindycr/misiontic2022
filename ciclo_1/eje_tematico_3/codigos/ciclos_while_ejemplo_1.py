"""
Estructuras iterativas en Python
Imprimir los números del 0 al 10 mediante un ciclo while
"""
# inicialización de variable
i = 0
# ciclo while
while i <= 10:
    # imprime variable i
    print(i)
    # cambia valor de variable i
    # si no se cambia i siempre será 0 generando un ciclo while infinito
    i = i+1
