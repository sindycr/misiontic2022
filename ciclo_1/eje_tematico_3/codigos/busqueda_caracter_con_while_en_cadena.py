"""
Programa para buscar un caracter dentro
de una cadena de caracteres.
"""
cadena = """Loremre"""
caracter = "o"
encontrado = False
longitud_cadena = len(cadena)
i = 0
while i < longitud_cadena:
    if cadena[i] == caracter:
        encontrado = True
        break
    i += 1

if encontrado:
    print(f"Se encontró el caracter {caracter} en cadena!")
else:
    print(f"No se encontró el caracter {caracter} en cadena!")
