"""
Estructuras iterativas en Python
Imprimir los números del 0 al 10 mediante un ciclo for
"""
# ciclo for
for i in range(11):
    # imprime variable i
    print(i)
