"""
Estructuras iterativas y variables de control en Python
Bucle for con contador
se tiene una lista en donde están almacenados los puntajes de la nota final
de los estudiantes. Se requiere discriminar entre los estudiantes que pasaron
el curso (nota igual o superior a 3.0) y los que no. Almacenar en una variable
el número de estudiantes que pasaron el curso y calcule el número de los que
no lo pasaron. Almacene en otra variable el promedio de la nota de todos los
estudiantes.
"""
# notas
notas = [2.0, 4.5, 3.9, 4.8, 3.1, 3.0, 3.7, 4.1, 4.3, 4.9,
         1.5, 2.9, 2.4, 4.3, 4.1, 4.0, 3.7, 3.9, 3.2, 3.0,
         4.6, 4.4, 3.4, 3.8, 3.2, 3.9, 2.5, 3.3, 4.7, 4.9,
         2.8, 4.0, 3.5, 3.5, 3.7, 3.1, 3.3, 4.6, 4.7, 3.8]
# número de estudiantes
numero_estudiantes = len(notas)
# número de estudiantes que pasaron
numero_estudiantes_aprobados = 0
suma_notas = 0
# ciclo for
for nota in notas:
    # se pide ingresar el producto
    if nota >= 3.0:
        # agrega valor a numero de estudiantes aprobados
        numero_estudiantes_aprobados += 1
    # agrega nota a suma de notas
    suma_notas += nota
# calcula el promedio
promedio = suma_notas / numero_estudiantes
# imprime el número de estudiantes que aprobaron
print(f"Aprobaron {numero_estudiantes_aprobados} estudiantes de {numero_estudiantes}")
print(f"El la nota promedio fue de {promedio}")
