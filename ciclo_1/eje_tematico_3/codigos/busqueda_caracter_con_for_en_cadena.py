cadena = """Loremre"""
caracter = "r"
encontrado = False

for c in cadena:
    if c == caracter:
        encontrado = True
        break

if encontrado:
    print(f"Se encontró el caracter {caracter} en cadena!")
else:
    print(f"No se encontró el caracter {caracter} en cadena!")

