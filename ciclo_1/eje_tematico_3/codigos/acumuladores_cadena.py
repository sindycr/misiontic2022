"""
Programa para acumular dentro de una cadena de caracteres
"""
# se define variable donde se almacenarán los datos, en este caso una cadena de caracteres
mercado = ""
# se pide el número de productos mediante entrada estándar
numero_productos = int(input("ingrese numero de productos: "))
# se inicia el ciclo for de acuerdo al número de productos
for i in range(numero_productos):
    # se pide el producto por entrada estándar
    producto = input(f"ingrese producto {i+1}: ")
    # se almacena en la variable mercado mediante concatenación de cadena de caracteres
    # se incluye separación por coma
    mercado = mercado + producto + ", "

# se imprime el mercado
print(f"mercado: {mercado}")
