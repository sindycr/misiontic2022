"""
Comparacion de dos cadenas
"""
# entrada de texto 1
texto_1 = input("ingrese el texto 1: ")
# entrada de texto 2
texto_2 = input("ingrese el texto 2: ")

# longitud de texto 1
len_t1 = len(texto_1)
# longitud de texto 2
len_t2 = len(texto_2)

# bandera que determina si los textos son iguales
son_iguales = True

# comparacion del tamaño de los textos
if len_t1 == len_t2:
    # si el tamaño es igual se crea ciclo while
    i = 0
    while i < len_t1:
        # se compara caracter por caracter
        if texto_1[i] != texto_2[i]:
            # si los caracteres son diferentes se cambia estado de bandera
            son_iguales = False
            # se rompe el ciclo while
            break
        i = i + 1
else:
    # si los tamaños de los textos son diferentes se cambia estado de bandera
    son_iguales = False

# se imprime resultado
print(f"los textos son iguales: {son_iguales}")
