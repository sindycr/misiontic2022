"""
Estructuras iterativas y variables de control en Python
Bucle for con acumulador
se desean guardar ciertos valores en una lista en la medida
que se va iterando un ciclo for
"""
# define variable acumuladora
mercado = []
# número de productos a guardar
numero_productos = 3
# ciclo for
for i in range(numero_productos):
    # se pide ingresar el producto
    producto = input(f"producto {i+1}: ")
    # se agrega a la variable acumuladora
    mercado.append(producto)

# fuera del ciclo se imprime la variable mercado
print(mercado)
