"""
Estructuras iterativas y variables de control en Python
Imprimir los números enteros iniciando desde cero mediante
un ciclo while, y se detiene al encontrar cierto número.
"""
# definición de bandera
bandera = True
# valor a incrementar en cada iteración
valor = 0
# valor a buscar
buscar = 10
# ciclo while
while bandera:
    # estructura de control condicional
    # determina si i en algún momento toma el valor del "valor"
    if valor == buscar:
        # se cambia el valor de la bandera
        bandera = False
    # imprime variable i
    print(valor)
    # incrementa valor en una unidad
    valor += 1
