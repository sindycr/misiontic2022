"""
Definición de variables en Python
"""
# cadena de caracteres
nombre = 'Ángela'
# enteros
edad = 27
# flotante
altura = 1.76
# Booleanas
es_estudiante = True
# Listas
notas = [3.5, 4.0, 4.2, 3.7]
