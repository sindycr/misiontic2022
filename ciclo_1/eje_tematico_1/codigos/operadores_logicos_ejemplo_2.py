"""
Operadores lógicos en Python
Operador or
"""
# almacenamiento de la operación lógica True or True
condicion_a = True or True
# impresión del resultado de la operación anterior
print(condicion_a)
# almacenamiento de la operación lógica True or False
condicion_b = True or False
# impresión del resultado de la operación anterior
print(condicion_b)
# almacenamiento de la operación lógica False or False
condicion_c = False or False
# impresión del resultado de la operación anterior
print(condicion_c)
# almacenamiento de la operación lógica False or True
condicion_d = False or True
# impresión del resultado de la operación anterior
print(condicion_d)
