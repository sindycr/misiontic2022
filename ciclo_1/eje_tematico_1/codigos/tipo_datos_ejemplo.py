"""
Uso de la función type() para verificar el tipo de dato en Python
"""
# Definición de variable para almacenar la marca de un auto
marca = "Kia Rio"
# Definición de variable para almacenar el modelo del auto
modelo = 2018
# variable para almacenar la placa
placa = "HKG-503"
# variable para almacenar si el vehículo está inmovilizado
vehiculo_inmovilizado = False
# visualiza el tipo de dato de marca
print(type(marca))
# visualiza el tipo de dato de modelo
print(type(modelo))
# visualiza el tipo de dato de placa
print(type(placa))
# visualiza el tipo de dato de vehiculo_inmovilizado
print(type(vehiculo_inmovilizado))
