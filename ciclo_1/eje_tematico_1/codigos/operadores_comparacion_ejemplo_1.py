"""
Operadores de comparación en Python
Operadores igualdad (==) y diferencia (!=)
"""
# definición de variable entera
numero_a = 10
# definición de variable entera
numero_b = 3
# Impresión del resultado de la comparación entre dos números enteros
# Si los números son iguales el resultado es True, de lo contrario es False
print(numero_a == numero_b)
# Impresión del resultado de la comparación entre dos números enteros
# Si los números son diferentes el resultado es True, de lo contrario es False
print(numero_a != numero_b)
