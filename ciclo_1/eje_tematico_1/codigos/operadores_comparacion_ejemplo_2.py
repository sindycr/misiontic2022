"""
Operadores de comparación en Python
Operadores mayor que (>) y menor que (<)
"""
# definición de variable numérica
numero_a = 10
# definición de variable numérica
numero_b = 3
# Impresión del resultado de la comparación entre dos números
# Si numero_a es mayor que numero_b el resultado es True, de lo contrario es False
print(numero_a > numero_b)
# Impresión del resultado de la comparación entre dos números
# Si numero_a es menor que numero_b el resultado es True, de lo contrario es False
print(numero_a < numero_b)
