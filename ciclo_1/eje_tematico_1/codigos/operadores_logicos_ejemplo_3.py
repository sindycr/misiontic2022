"""
Operadores lógicos en Python
Operador not
"""
# almacenamiento de la operación not True
condicion_a = not True
# impresión del resultado de la operación anterior
print(condicion_a)
# almacenamiento de la operación not False
condicion_b = not False
# impresión del resultado de la operación anterior
print(condicion_b)
