"""
Operadores aritméticos en Python
Operadores división entera (//) y potenciación (**)
"""
# Define variable entera o flotante
numero_a = 10
# Define variable entera o flotante
numero_b = 3
# Suma de dos variables que pueden ser enteras o flotante o combinadas
numero_c = numero_a + numero_b
# Resta de dos variables que pueden ser enteras o flotante o combinadas
numero_d = numero_a – numero_b
# definición de variable a partir de la operación de división entera
numero_e = numero_c // numero_d
# equivalente a
numero_e = int(numero_c / numero_d)
# Imprime el valor obtenido de la división entera
print(numero_e)
# Define variable usando el operador potenciación
numero_f = numero_c ** 2
# equivalente a
numero_f = numero_c * numero_c
# Imprime el valor obtenido de la potenciación
print(numero_f)
