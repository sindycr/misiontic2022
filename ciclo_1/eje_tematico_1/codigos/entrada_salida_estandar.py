"""
Entrada y salida estándar en Python
"""
# definición de variables usando entrada estándar
nombre = input()
edad = input()
# salida estándar
print(nombre)
print(edad)
