"""
Definición de variables enteras en Python
"""
# Usando números binarios, base 2
# definición del número 20
numero_datos = 0b10100
# Usando números hexadecimales, base 16
# definición del número 20
numero_datos = 0x14
# Usando números octales, base 8
# definición del número 20
numero_datos = 0o24
