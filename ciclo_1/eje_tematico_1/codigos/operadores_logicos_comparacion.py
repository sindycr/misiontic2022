"""
Operadores lógicos en Python
Operador not y operador comparación (==)
"""
# Comparación entre variable booleana True y valor entero
condicion_a = True == 1
print(condicion_a)
# Comparación entre variable booleana False y valor entero
condicion_b = False == 0
print(condicion_b)
# Comparación entre la negación de variable booleana False y valor entero
condicion_c = not False == 1
print(condicion_c)
# Comparación entre la negación de variable booleana True y valor entero
condicion_d = not True == 0
print(condicion_d)
# Comparación de valor entero y valor flotante
condicion_e = 4 == 4.0
print(condicion_e)
