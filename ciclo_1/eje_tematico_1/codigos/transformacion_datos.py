"""
Transformación de datos en Python
"""
# Transformación de cadena de caracteres a entero
edad = "28"
edad = int(edad)
# Transformación de flotante a cadena de caracteres
temperatura = 45.0
temperatura = str(temperatura)
# Transformación de cadena de caracteres a flotante
velocidad = "98.5"
velocidad = float(velocidad)
