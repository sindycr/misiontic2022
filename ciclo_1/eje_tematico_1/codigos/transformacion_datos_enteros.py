"""
Transformación de tipos de datos enteros en Python
"""
# recolección de información por entrada estándar y almacenamiento en variable edad
edad = input()
# se imprime el dato tipo cadena de caracteres o str
print(type(edad))
# transformación de dato tipo cadena de caracteres recolectado por medio de entrada
# estándar y almacenamiento en variable edad como dato tipo entero usando la función
# int
edad = int(input())
# se imprime el dato tipo entero o int
print(type(edad))
