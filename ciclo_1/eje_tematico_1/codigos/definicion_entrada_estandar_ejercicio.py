"""
Ejercicio de definición de variables usando entrada estándar en Python
"""
# definición de variable para almacenar la marca del auto
marca = input()
# definición de variable para almacenar el modelo del auto
modelo = input()
# definición de variable para almacenar la placa
placa = input()
# definición de variable para almacenar el documento de identificación
documento_id = input()
# definición de variable para almacenar el número de infracciones del vehículo
infracciones = input()
# definición de variable booleana para indicar si el auto está inmovilizado
vehiculo_inmovilizado = False
