"""
Operadores aritméticos en Python
Operador módulo asociado con el residuo de una división
"""
# Define variable entera o flotante
numero_a = 10
# Define variable entera o flotante
numero_b = 3
# Definición de variable usando el operador módulo
# retorna el residuo de la división entre numero_a y numero_b
numero_e = numero_a % numero_b
# Imprime el resultado de la operación usando el operador módulo
print(numero_e)
# Definición de variable usando el operador módulo
# retorna el residuo de la división entre numero_a y numero_b
numero_f = numero_a % 2
# Impresión del resultado de la operación usando el operador módulo
# Se imprime el residuo de la división entre numero_a y 2
# esto permite indicar cuando un número es divisible por 2 o es par
# si el resultado es cero es par de lo contrario es impar
print(numero_f)
