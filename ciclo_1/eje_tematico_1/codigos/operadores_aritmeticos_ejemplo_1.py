"""
Operadores aritméticos en Python
Operadores suma (+) y resta (-)
"""
# Define variable entera o flotante
numero_a = 10
# Define variable entera o flotante
numero_b = 3
# Suma de dos variables que pueden ser enteras o flotante o combinadas
numero_c = numero_a + numero_b
# Imprime resultado de la suma
print(numero_c)

# Resta de dos variables que pueden ser enteras o flotante o combinadas
numero_d = numero_a – numero_b
print(numero_d)
