"""
Operadores de comparación en Python
Operadores mayor o igual que (>=) y menor igual que (<=)
"""
# definición de variable numérica
numero_a = 10
# definición de variable numérica
numero_b = 10
# Impresión del resultado de la comparación entre dos números
# Si numero_a es mayor o igual que numero_b el resultado es True, de lo contrario es False
print(numero_a >= numero_b)
# Impresión del resultado de la comparación entre dos números
# Si numero_a es menor o igual que numero_b el resultado es True, de lo contrario es False
print(numero_a <= numero_b)
