"""
Definición de variables flotante en Python
"""
# Definición del número pi
numero_pi = 3.14159
# Definición de la velocidad de la luz
# Usando notación convencional
velocidad_luz = 299792458.0
# Usando notación científica
velocidad_luz = 2.99792458e8
