"""
Función sin argumentos ni retorno
"""


# definición de la función
def imprime_mensaje():
    print("Hola a todos!")


# uso de la función
imprime_mensaje()
