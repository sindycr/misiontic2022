"""
Función con argumento y retorno, función para transformar unidades de temperatura
Fahrenheit to Celsius
"""


# definición de la función
def fahrenheit2celsius(temperatura_fahrenheit):
    temperatura_celsius = (temperatura_fahrenheit - 32) * 5 / 9
    return temperatura_celsius


# uso de la función
fahrenheit2celsius(90.0)
