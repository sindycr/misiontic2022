"""
Función con argumentos y sin retorno
"""


# definición de la función
def imprime_mensaje(mensaje):
    print(f"Mensaje: {mensaje}")


# uso de la función
imprime_mensaje("Hola a todos!")
