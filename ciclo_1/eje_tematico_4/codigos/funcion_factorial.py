"""
Funciones recursivas en Python
Función factorial

si el número es 3, su factorial es 3x2x1
si el número es 4, su factorial es 4x3x2x1
si el número es 5, su factorial es 5x4x3x2x1
si el número es n, su factorial es nx(n-1)x(n-1)x...x3x2x1

"""
# función factorial
def factorial(n):
    # si el valor de n es 0 o 1, el factorial es 1
    if n == 1 or n == 0:
        # retorna 1
        return 1
    else:
        # de lo contrario recurre a la función hasta que
        # el argumento de la función factorial sea 1
        return n * factorial(n-1)
