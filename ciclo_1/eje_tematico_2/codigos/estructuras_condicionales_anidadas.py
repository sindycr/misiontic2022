"""
Estructuras de control anidadas en Python
Uso de if, else:
Se desea comparar el número de items que se desea comprar de determinado producto con el número de items que se
tienen de inventario. Si la cantidad de items a comprar es mayor a la inventariada se debe mostrar un mensaje
solicitando suplir ese producto. Si hay suficientes productos se debe solicitar empacar los productos en grupos
de 4 productos por paquete, si son más de 1 paquete se debe imprimir que se requiere más de un paquete para
realizar el pedido.
"""
# define una variable para almacenar el número de items que se desean comprar
items_compra = 20
# define una variable para almacenar el número de items que se tienen de inventario
items_inventario = 23
# se visualiza la información de la compra
print(f"Se desea comprar {items_compra} items y se tiene en inventario {items_inventario} items.")
# valor de la variable aprobado si se cumple la condición
if items_compra >= items_inventario:
    # imprime aviso solicitando suplir el producto
    print("Se debe suplir del producto solicitado!")
else:
    numero_paquetes = int(items_compra / 4)
    if numero_paquetes > 1:
        # imprime que se requieren multiples paquetes
        print(f"se requieren {numero_paquetes} paquetes para realizar el pedido")
