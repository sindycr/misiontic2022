"""
Estructuras de control en Python
Uso de if con multiples condiciones usando operaciones lógicas:
Se desea determinar si el valor de la temperatura está entre ciertos valores.
"""
# define una variable para almacenar la temperatura
temperatura = 55.0
temperatura_optima = 60
# define una estructura de control condicional que indique cuando el valor
# de la temperatura es menor que 60, cuando es igual a 60 y cuando es mayor que 60
if temperatura < temperatura_optima:
    print("por debajo de la temperatura optima")
elif temperatura == temperatura_optima:
    print("temperatura optima")
elif temperatura > temperatura_optima:
    print("por encima de la temperatura optima")
