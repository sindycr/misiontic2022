"""
Estructuras de control en Python
Uso de if con multiples condiciones usando operaciones lógicas:
Se desea determinar cuando un número está dentro de cierto rango de valores.
"""
# define una variable para almacenar un valor numérico
n = 25.0
# define una estructura de control condicional que indique cuando el valor
# de n está entre los valores 18 y 26. Se imprime "valor dentro del rango"
# si n está dentro del rango de valores, de lo contrario imprime
# "valor fuera de rango". Se puede simplificar la condición n >= 18 and n <= 26
# como 18 <= n <= 26
if 18 <= n <= 26:
    print("valor dentro del rango")
else:
    print("valor fuera de rango")
