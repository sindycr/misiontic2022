"""
Manipulación de archivos JSON en Python
"""

# se requiere el módulo json
import json

if __name__ == "__main__":
    # apertura de archivos JSON
    # se requiere que exista en alguna ruta el archivo JSON que se desea abrir
    with open("archivos/estudiantes.json") as f:
        # el archivo se puede leer en modo texto como f.read()
        data = json.load(f)

    # se puede visualizar el contenido de data
    print(data)

    # si el dato JSON almacena una lista podemos leer dato por dato usando un ciclo
    for estudiante in data:
        print(estudiante)

    # si se alteran los datos (crear función que calcule el promedio y agregue a
    # cada diccionario la llave promedio)

    # se guardan los datos modificados de la siguiente manera
    with open("archivos/estudiantes_modificados.json", "w") as f:
        # se usa el módulo json para darle el formato adecuado a los datos
        # y guardar
        json.dump(data, f)
