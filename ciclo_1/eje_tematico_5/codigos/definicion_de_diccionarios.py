"""
Definición de diccionarios en Python
"""
# cadenas como llaves
dic1 = {"llave1": 0, "llave2": 1, "llave3": 2}
dic2 = {"lista": [8, 9, 10], "llave": 30}
# enteros como llaves
dic3 = {0: "valor A", 1: "valor B", 2: "valor C"}
# booleanos como llaves
dic4 = {True: 1, False: 2}
# flotantes como llaves
dic5 = {2.8: "valor", "llave": 10}
# diccionario vacío
dic6 = {}

# acceso
print(dic1["llave1"])
print(dic2["lista"])
print(dic3[1])
print(dic4[False])
print(dic5[2.8])
