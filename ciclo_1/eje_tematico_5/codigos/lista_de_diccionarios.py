"""
Definición de diccionarios en Python
"""
# diccionario ejemplo
dic1 = {"item": "Item 1", "cantidad": 3, "precio": 34000}

# lista de diccionarios
items = [
    {"item": "Item 1", "cantidad": 3, "precio": 34000},
    {"item": "Item 2", "cantidad": 2, "precio": 25000},
    {"item": "Item 3", "cantidad": 1, "precio": 18000}
]

# visualización
print(items)
print(item[0])
print(item[1]["item"])
