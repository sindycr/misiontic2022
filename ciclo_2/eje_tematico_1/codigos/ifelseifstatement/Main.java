/*
 * Codigo que determina si una persona está en la primera infancia, infancia, juventud, adultez o vejez
 */

// importe de modulo Scanner
import java.util.Scanner;

/**
 *
 * @author Alberto Silva
 */
public class Main {

    public static void main(String[] args) {
        // definicion de constante
        int edad;
        // instancia de clase Scanner
        Scanner in = new Scanner(System.in);
        // almacenamiento de la edad mediante entrada estandar
        System.out.print("cuantos años tiene?: ");
        edad = in.nextInt();
        // se verifica pertenece a la primera infancia, infancia, adolescencia, juventud, adultez o vejez
        if (edad >= 0 && edad <= 5){
            System.out.println("Primera infancia");
        } else if (edad >= 6 && edad <= 11) {
            System.out.println("Infancia");
        } else if (edad >= 18 && edad <= 26) {
            System.out.println("Juventud");
        } else if (edad >= 27 && edad <= 59) {
            System.out.println("Adultez");
        } else {
            System.out.println("Vejez");
        }
    }
}
