/*
 * ciclo do while
 */

/**
 *
 * @author Alberto Silva
 */
public class Main {

    public static void main(String[] args) {
        // se inicializa i=0
        int i=0;
        // ciclo do while, hasta que no se cumpla i<5
        do {
            // se imprime el valor de i
            System.out.println(i);
            // se incrementa i de uno en uno (i++)
            i++;
        } while (i < 5);
    }
}
