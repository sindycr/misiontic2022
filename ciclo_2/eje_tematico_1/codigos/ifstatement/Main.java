/*
 * Codigo que determina si una persona es mayor de edad segun su edad
 */

// importe de modulo Scanner
import java.util.Scanner;

/**
 *
 * @author Alberto Silva
 */
public class Main {

    public static void main(String[] args) {
        // definicion de constante
        final int edadLimite = 18;
        int edad;
        // instancia de clase Scanner
        Scanner in = new Scanner(System.in);
        // almacenamiento de la edad mediante entrada estandar
        System.out.print("cuantos años tienes?: ");
        edad = in.nextInt();
        // se verifica si la persona es mayor de edad
        if (edad >= edadLimite) {
            System.out.println("La persona es mayor de edad!");
        } else {
            System.out.println("La persona es menor de edad!");
        }
    }
}
