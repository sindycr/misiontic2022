/*
 * Algunas operaciones logicas en Java
 */

// importe de modulo Scanner
import java.util.Scanner;

/**
 *
 * @author Alberto Silva
 */
public class Main {

    public static void main(String[] args) {
        // definicion de constante
        final int maximosDatos = 50;
        // instancia de clase Scanner
        Scanner in = new Scanner(System.in);
        // definicion de variables mediante entrada estandar
        System.out.print("cuantos datos son?: ");
        int numeroDatos = in.nextInt();
        // se verifica si el valor ingresado es negativo o nulo
        boolean esInvalido = numeroDatos < 0;
        boolean sonIguales = numeroDatos == maximosDatos;
        boolean sonDiferentes = numeroDatos != maximosDatos;
        // se imprime el valor de la variable booleana
        System.out.println("valor negativo o nulo: " + esInvalido);
        System.out.println("numeroDatos es igual a maximosDatos: " + sonIguales);
        System.out.println("numeroDatos es diferente a maximosDatos: " + sonDiferentes);
        // se compara el dato ingresado y se almacena el dato booleano
        boolean muchosDatos = numeroDatos > maximosDatos;
        // se imprime el valor de la variable booleana
        System.out.println("son muchos datos?: " + muchosDatos);
    }
}
