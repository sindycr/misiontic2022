/*
 * ciclo for
 */

/**
 *
 * @author Alberto Silva
 */
public class Main {

    public static void main(String[] args) {
        // ciclo for, inicializado en i=0, hasta que no se cumpla i<5, y va incrementando i de uno en uno (i++)
        for (int i=0; i<5; i++) {
            // se imprime el valor de i
            System.out.println(i);
        }
    }
}
