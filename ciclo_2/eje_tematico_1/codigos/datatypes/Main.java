/*
 * Clase principal Main
 *
 * Declaracion y definicion de variables en Java
 * Importe de paquetes
 * Uso de clase externa para la creacion de instancias
 */

import constants.*;

/**
 *
 * @author Alberto Silva
 */
public class Main {
    
    public static void main(String[] args) {
        // delcaracion de variables
        // variable tipo flotante
        float temperatura;
        // inicializacion
        temperatura = 34.0f;
        // declaracion e inicializacion de variables
        int numeroIteraciones = 20;
        // declaracion e inicializacion de multiples variables
        double x = 12.5, y = 21.2, z = 0.0;
        // instancia de clase claseB
        Constants obj = new Constants();
        float temp = obj.temperaturaAmbiente;
        int max_iter = obj.ITERACIONES_MAXIMAS;
    }
}
