/*
 * Algunas operaciones booleanas en Java
 * Se requiere detectar cuando un punto x, y esta dentro o fuera de un area delimitada
 * por un espacio que en una direccion va desde xa a xb y en la otra direccion va
 * desde ya a yb.
 */

/**
 *
 * @author Alberto Silva
 */
public class Main {

    public static void main(String[] args) {
        String edad = "32.6";
        float valor = Float.parseFloat(edad);
        System.out.println(valor);
        // definicion de variables
        float x = 1.5f, y = 2.0f;
        // definicion de constantes
        final float xa = 1.0f, xb = 3.0f, ya = 1.0f, yb = 4.0f;
        // se verifica si el punto x,y esta entre xa y xb con operador and (&&)
        boolean estaEnRangox = x >= xa && x <= xb;
        boolean estaEnRangoy = y >= ya && y <= yb;
        // se determina si ambas expresiones booleanas son verdaderas con operador and (&&)
        boolean estaDentroDeArea = estaEnRangox && estaEnRangoy;
        // se verifica si el punto x,y esta fuera de xa y xb con operador or (||)
        boolean estaFueraDeRangox = x < xa || x > xb;
        boolean estaFueraDeRangoy = y < ya || y > yb;
        // se determina si alguna de las expresiones booleanas es verdadera con operador and (||)
        boolean estaFueraDeArea = estaFueraDeRangox || estaFueraDeRangoy;
        // se puede negar una expresion booleana con el operador not (!)
        boolean negacionDeEstaFueraDeArea = !estaFueraDeArea;
        // se imprimen los valor de las variables booleanas
        System.out.println("El punto esta dentro del area delimitada: " + estaDentroDeArea);
        System.out.println("El punto esta fuera del area delimitada: " + estaFueraDeArea);
        System.out.println("Negacion de estaFueraDeArea: " + negacionDeEstaFueraDeArea);
    }
}
