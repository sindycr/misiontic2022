/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package grouplayoutexample;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author betox
 */
public class GroupLayoutExample {

    public static void main(String[] args) {
        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame("Group Layout");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JButton b1 = new JButton("Boton 1");
        JButton b2 = new JButton("Boton 2");
        JButton b3 = new JButton("Boton 3");
        JButton b4 = new JButton("Boton 4");
        
        JPanel panel = new JPanel();
        GroupLayout layout = new GroupLayout(panel);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        panel.setLayout(layout);
        
        // grupos verticales y horizontales
        layout.setHorizontalGroup(
                layout.createSequentialGroup().addComponent(b1).addComponent(b2)
                .addGroup(layout.createSequentialGroup().addGroup(layout
                .createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(b3).addComponent(b4)))
        );
        layout.setVerticalGroup(
                layout.createSequentialGroup().addComponent(b1).addComponent(b2).addComponent(b3).addComponent(b4)
        );
        
        frame.add(panel);
        frame.pack();
        frame.setVisible(true);
    }
}
