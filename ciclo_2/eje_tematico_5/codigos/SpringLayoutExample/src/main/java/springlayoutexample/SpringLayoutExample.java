/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package springlayoutexample;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

/**
 *
 * @author betox
 */
public class SpringLayoutExample {

    public static void main(String[] args) {
        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame("Spring Layout");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JLabel label = new JLabel("Etiqueta: ");
        JTextField campoTexto = new JTextField("Texto ...", 15);
        
        JPanel panel = new JPanel();
        SpringLayout layout = new SpringLayout();
        
        layout.putConstraint(SpringLayout.WEST, label, 5, SpringLayout.WEST, panel);
        layout.putConstraint(SpringLayout.NORTH, label, 5, SpringLayout.NORTH, panel);
        //layout.putConstraint(SpringLayout.WEST, campoTexto, 6, SpringLayout.WEST, panel);
        //layout.putConstraint(SpringLayout.NORTH, campoTexto, 6, SpringLayout.NORTH, panel);
        panel.setLayout(layout);
        
        panel.add(label);
        panel.add(campoTexto);
        
        frame.add(panel);
        frame.pack();
        frame.setVisible(true);
    }
}
